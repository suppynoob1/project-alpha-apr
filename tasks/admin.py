from django.contrib import admin
from projects.models import Project
from tasks.models import Task


# Register your models here.
@admin.register(Project)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
    )


@admin.register(Task)
class TasksAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
